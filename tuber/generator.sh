#!/bin/bash

stub3on4() {
    echo '// BOF'
    echo ''
    echo 'include <tuber3on4.scad>;'
    echo ''
    echo 'height = hhh;'
    echo ''
    echo '// EOF'
}


stub2on3() {
    echo '// BOF'
    echo ''
    echo 'include <tuber2on3.scad>;'
    echo ''
    echo 'height = hhh;'
    echo ''
    echo '// EOF'
}


prefix=""
base="tuber"

for h in 1 2 3 4 5 6 ; do 
    stub3on4 | sed "s/hhh/$h/" > ${prefix}${base}3on4-${h}.scad
    stub2on3 | sed "s/hhh/$h/" > ${prefix}${base}2on3-${h}.scad
done

#EOF
