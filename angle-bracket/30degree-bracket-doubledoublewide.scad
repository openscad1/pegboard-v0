//BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

chamfer = wall_thickness / 4;

translate([-wall_thickness - 0.01, 0, base_size]) {
    rotate([0, 0, -90]) {
        rail(2, copies = 4, side_wall_gap = default_back_wall_gap, chamfer = chamfer);
    }
}



module blade() {
    // fin
    //
    color("orange") {
        hull() {
            rotate([0, 30, 0]) {
                chamfered_box(dim = [wall_thickness, wall_thickness * 2, base_size * 3], align = [-1, 0, 1], chamfer = chamfer);
            }
            chamfered_box(dim = [wall_thickness, wall_thickness * 2, base_size * 3], align = [-1, 0, 1], chamfer = chamfer);
        }
    }


    // rail head
    //
    rotate([0, 30, 0]) {
        color("salmon") {
            chamfered_box(dim = [wall_thickness, base_size - (wall_thickness * 2) - (default_back_wall_gap * 2), base_size * 3], align = [-1, 0, 1], chamfer = chamfer);
        }
    }
}

translate([0, -base_size, 0])
blade();


translate([0, base_size, 0])
blade();


//EOF
