use <moscube/moscube.scad>;

fine = true; 
//fine = false; 

$fn = fine ? 25 : 12;

roundcorner_radius = 1; 

module brick_slug() {
    moscube([96, 28, 60], roundcorner_radius = roundcorner_radius, fn = 25);
}

brick_slug();
