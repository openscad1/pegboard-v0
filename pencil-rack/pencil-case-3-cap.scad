include <pencil-case.scad>;
// default from pencil-case.scad (inside of cap tube): gap = 0.25;
// default from pencil-case.scad (outside of base inner tube): outer_gap = 0;

gap = 0.5;

width_factor = 3;
do_cap = true;
do_base = false;

render();
