// BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

include <ruler-holder.scad>;

height_factor = 2;
length_factor = 2.1 + (wall_thickness * 4 / base_size);
width_factor = 1;

slug_x = 0.5;
slug_y = 2.1;
slug_z = 3.5;


render_ruler_holder();

module slug() {
    translate([0, wall_thickness * 2, 0]) {
        color("purple") {
            translate([0, (slug_y / 2) * base_size, (slug_z / 2) * base_size + wall_thickness]) {
                cube([base_size * slug_x, base_size * slug_y, base_size * slug_z], center =  true);
            }
        }
    }
}


// true
// false

if(false) {
    slug();
}




// EOF
