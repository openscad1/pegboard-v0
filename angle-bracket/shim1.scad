// BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

chamfer = wall_thickness / 4;

rail(1, chamfer = chamfer, topless = true);



// EOF
