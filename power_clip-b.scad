use <moscube/moscube.scad>;
use <lib/cover.scad>;



width = 26;
length = 62;
height = 15;

apeture_height = 10;
apeture_width = 18;

base_size = 25.4;
plate_thickness = base_size / 8;
roundcorner_radius = 1;
roundcorner_diameter = roundcorner_radius * 2;

$fn = 25;


switch_width = 43;
switch_length = 15;
switch_height = 12;

module faux_switch() {
    color("pink") {
        moscube([switch_length, switch_width, switch_height], roundcorner_radius = 1, z_anchor = 1);

        translate([0, 0, 12]) rotate([10, 0, 0]) {
            rotate([ 10, 0, 0]) moscube([6, 17, 3]);
            rotate([-10, 0, 0]) moscube([6, 17, 3]);
                                moscube([6, 17, 5], z_anchor = -1);
        }
    }
}


// calibration cube
//translate([0, 23, 0]) color("red") moscube([4,4,4], roundcorner_radius = 1);

module switch_clip(bogus = false) {
    // switch clip plate & fingers
    bogosity = bogus ? plate_thickness / 2 : 0;
    color("cornflowerblue") {
        if(!bogus) {
            translate([0, 0, bogosity]) {
                moscube([switch_length + (plate_thickness * 2), (switch_width + (plate_thickness * 2)) / 2, plate_thickness], roundcorner_radius = 1, y_anchor = 1, z_anchor = -1);
            }
        }
        for(xpol = [-1, 1]) {
            for(ypol = [1]) {
                translate([0, 0, - (plate_thickness - bogosity)]) {
                    translate([xpol * ((switch_length + plate_thickness) / 2), ypol * ((switch_width / 2) - plate_thickness), 0]) {
                        moscube([plate_thickness, plate_thickness * 4, switch_height + plate_thickness * 2 - bogosity], roundcorner_radius = 1, z_anchor = 1);
                    }
                    translate([xpol * ((switch_length - plate_thickness) / 2), ypol * ((switch_width + plate_thickness) / 2), 0]) {
                        moscube([plate_thickness * 2.5, plate_thickness, switch_height + plate_thickness * 2 - bogosity], roundcorner_radius = 1, z_anchor = 1);
                    }
                    translate([xpol * ((switch_length - plate_thickness) / 2), ypol * (switch_width + plate_thickness * 2) / 2, switch_height + plate_thickness - bogosity]) {
                        moscube([plate_thickness * 2.5, plate_thickness * 4, plate_thickness], roundcorner_radius = 1, y_anchor = -ypol, z_anchor = 1);
                    }
                }
            }
        }
    }
}



//faux_switch();
switch_clip();
rotate([0, 0, 180]) {
    switch_clip(bogus=true);
}
