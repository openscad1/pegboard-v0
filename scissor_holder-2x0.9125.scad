use <lib/loop.scad>;

fine = true;
//fine = false;

$fn = fine ? 25 : 12;


loop2(size = 0.9125, fn = $fn);

/*
difference() {
translate([0, -23.25, 5]) 
    rotate([90, 0, 0])
        loop2(size = 0.9125, fn = $fn);

color("cyan") loop3(width = 1, depth = 0.9125 * 2, fn = $fn);
}
//color("orange") cube([25.4, 25.4, 100], center = true);

*/

