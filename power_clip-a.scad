use <moscube/moscube.scad>;
use <lib/cover.scad>;



width = 26;
length = 62;
height = 15;

apeture_height = 10;
apeture_width = 18;

base_size = 25.4;
plate_thickness = base_size / 8;
roundcorner_radius = 1;
roundcorner_diameter = roundcorner_radius * 2;

$fn = 25;


//translate([0,0,(height + (plate_thickness * 2))/2]) moscube([apeture_height,apeture_width,apeture_height], roundcorner_radius = 2);

/*
translate([0, 0, (height + (plate_thickness * 2)) / 2]) {
//    minkowski() {


        difference() {
            translate([0, 0, 0]) color("red") moscube([length/2, width + (plate_thickness * 2), height + (plate_thickness * 2)]);
            translate([-((length / 4) - plate_thickness), 0, 0]) color("blue") moscube([length, width, height], x_anchor = 1);
            translate([0, 0, 0]) color("orange") moscube([length, apeture_width, apeture_height]);

            // clip the top
            moscube([100, 100, 100], z_anchor = 1);

        }

//        sphere(r = roundcorner_radius);

//    }
}

*/


translate([0, 0, (height + (plate_thickness * 2)) / 2]) {
    minkowski() {


        difference() {
            translate([0, 0, 0]) color("red") moscube([length/2 - roundcorner_diameter, width + (plate_thickness * 2) - roundcorner_diameter, height + (plate_thickness * 2) - roundcorner_diameter]);
            translate([-((length / 4) - plate_thickness) - roundcorner_radius, 0, 0]) color("blue") moscube([length + roundcorner_diameter, width + roundcorner_diameter, height + roundcorner_diameter], x_anchor = 1);
            translate([0, 0, 0]) color("orange") moscube([length + roundcorner_diameter, apeture_width + roundcorner_diameter, apeture_height + roundcorner_diameter]);

            // clip the top
            //moscube([100, 100, 100], z_anchor = -1);

        }

        sphere(r = roundcorner_radius);

    }
}

/*

l = 3;

rotate([180, 0, 90])
translate([-base_size / 2, (-base_size / 2) * l , 0])
cover(1, l, 0.2);

rotate([0, 0, 90])
translate([-base_size / 2, (-base_size / 2) * l, 0])
cover(1, l, 0.2);

*/