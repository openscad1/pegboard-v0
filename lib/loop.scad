use <tube.scad>;
use <cover.scad>;
use <box.scad>;




    
module loopnc(size = 1.0, height = 2, roundcorner_radius = 1, fn = 0) {
    $fn = fn ? fn : $fn;

    base_size = 25.4;
    plate_thickness = base_size / 8;



    y_scale = 2;
    translate([0, 0, size * (-base_size / (2 / y_scale))]) {
        rotate([90,0,180]) {
            minkowski() {
                difference() {
                    scale([size, size * y_scale, 1]) {
                        color("orange") cylinder(d = base_size, h = (base_size * height) - 2, center = true);
                    }
                    scale([size, size * 2.125, 1]) {
                        color("cyan") cylinder(d = base_size - plate_thickness, h = base_size * (height + 1), center = true);
                    }
                }
                sphere(r = roundcorner_radius);
            }
        }
    }
}


    
module loop(size = 1.0, height = 2, roundcorner_radius = 1, fn = 0) {
    $fn = fn ? fn : $fn;

    base_size = 25.4;
    plate_thickness = base_size / 8;

    translate([-base_size / 2, -(base_size * height)/2, -(plate_thickness / 2)]) {
        cover(1, height, 0.2);
    }



    y_scale = 2;
    translate([0, 0, size * (-base_size / (2 / y_scale))]) {
        rotate([90,0,180]) {
            minkowski() {
                difference() {
                    scale([size, size * y_scale, 1]) {
                        color("orange") cylinder(d = base_size, h = (base_size * height) - 2, center = true);
                    }
                    scale([size, size * 2.125, 1]) {
                        color("cyan") cylinder(d = base_size - plate_thickness, h = base_size * (height + 1), center = true);
                    }
                }
                sphere(r = roundcorner_radius, $fn = 8);
            }
        }
    }
}

    
module loop2nc(size = 1.0, height = 2, roundcorner_radius = 1, fn = 0) {
    
    base_size = 25.4;
    plate_thickness = base_size / 8;
    $fn = fn ? fn : $fn;
    
 


    y_scale = 2;
    translate([0, 0, size * (-base_size / (2 / y_scale))]) {
        rotate([90,0,180]) {
            minkowski() {
                difference() {
                    scale([size, size * y_scale, 1]) {
                        color("orange") cylinder(d = base_size, h = (base_size * height) - 2, center = true);
                    }
                    scale([size, size * 2.125, 1]) {
                        color("cyan") cylinder(d = base_size - plate_thickness, h = base_size * (height + 1), center = true);
                    }
                    translate([0, -30, 45]) {
                        rotate([37.5, 0, 0]) {
                            cube([100,100,100], center = true);
                        }
                    }
                }
                sphere(r = roundcorner_radius);
            }
        }
    }
}

    
module loop2(size = 1.0, height = 2, roundcorner_radius = 1, fn = 10) {
    
    base_size = 25.4;
    plate_thickness = base_size / 8;
    $fn = fn;
    
    translate([-base_size / 2, -(base_size * height)/2, -(plate_thickness / 2)]) {
        cover(1, height, 0.2);
    }



    y_scale = 2;
    translate([0, 0, size * (-base_size / (2 / y_scale))]) {
        rotate([90,0,180]) {
            minkowski() {
                difference() {
                    scale([size, size * y_scale, 1]) {
                        color("orange") cylinder(d = base_size, h = (base_size * height) - 2, center = true);
                    }
                    scale([size, size * 2.125, 1]) {
                        color("cyan") cylinder(d = base_size - plate_thickness, h = base_size * (height + 1), center = true);
                    }
                    translate([0, -30, 45]) {
                        rotate([37.5, 0, 0]) {
                            cube([100,100,100], center = true);
                        }
                    }
                }
                sphere(r = roundcorner_radius);
            }
        }
    }
}


    
module loop3(width = 1.0, depth = 2.0, height = 2.0, roundcorner_radius = 1, fn = 10, cut = 0) {
    
    base_size = 25.4;
    plate_thickness = base_size / 8;
    $fn = fn;
    
    minkowski() {
    difference() {
        translate([0, 0, base_size * height / 2]) {
            difference() {
                color("orange") resize([width * base_size - (roundcorner_radius * 2), depth * base_size - (roundcorner_radius * 2), height * base_size - roundcorner_radius * 2]) {
                    cylinder(d = base_size, h = base_size, center = true);
                }
            
                color("fuchsia") resize([
                        (((base_size * width) - (plate_thickness * 2)) / (base_size * width)) * width * base_size + (roundcorner_radius * 2),
                        (((base_size * depth) - (plate_thickness * 2)) / (base_size * depth)) * depth * base_size + (roundcorner_radius * 2),
                        height * base_size
                    ]) {
                    cylinder(d = base_size, h = base_size, center = true);
                }
            }
        }

        if (cut) {
            opp = tan(cut) * (base_size * depth);
            translate([-base_size * width / 2, base_size * depth / 2, roundcorner_radius]) {
                rotate([-cut, 180, 180]) {
                    color("red") cube([base_size * width, base_size * depth * 2, base_size * height]);
                }
            }
        }


    }
    sphere(r = roundcorner_radius);
}
}



height = 2;
width = 1; 
depth = 2.5;
roundcorner_radius = 1;
cut = 30;

//loop3(roundcorner_radius = roundcorner_radius, width=width, depth=depth, height = height, cut = cut, fn = 12);

    base_size = 25.4;
    plate_thickness = base_size / 8;

//color("red") translate([(base_size - plate_thickness) / 2, 0, (base_size * height) / 2]) cube([plate_thickness, plate_thickness, plate_thickness], center = true);


