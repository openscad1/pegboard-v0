


module _chamfered_ring(od, id, chamfer) {
    rotate_extrude() 
        translate([id / 2, 0, 0])
            polygon(points = [
                [0, 0],
                [(od - id) / 2, 0], 
                [(od - id) / 2 - chamfer, chamfer],
                [chamfer, chamfer]
            ]);
}


module _chamfered_disc(od, chamfer) {
    rotate_extrude() 
        polygon(points = [
            [0, 0],
            [(od / 2), 0], 
            [(od / 2) - chamfer, chamfer],
            [0, chamfer]
        ]);
}



module _tube(height, od, id) {
    rotate_extrude() 
        translate([id / 2, 0, 0])
            square([(od - id) / 2, height]);
}


module chamfered_tube(height, od, id, chamfer ) {

    // bottom ring
    //
    translate([0, 0, chamfer]) {
        rotate([180, 0, 0]) {
            color("orange") {
                _chamfered_ring(od, id, chamfer);
            }
        }
    }
    
    // top ring
    //
    translate([0, 0, height - chamfer]) {
        color("pink") {
            _chamfered_ring(od, id, chamfer);
        }
    }
    
    // central tube
    //
    color("salmon") {
        translate([0, 0, chamfer - 0.001]) {
            _tube(height - (chamfer * 2) + (0.001 * 2), od, id);
        }
    }
}


module chamfered_cylinder(height, od, chamfer ) {
    
    // bottom disc
    //
    translate([0, 0, chamfer]) {
        rotate([180, 0, 0]) {
            color("orange") {
                _chamfered_disc(od, chamfer);
            }
        }
    }


    // top disc
    //
    translate([0, 0, height - chamfer]) {
        color("pink") {
            _chamfered_disc(od, chamfer);
        }
    }
    
    
    // central cylinder
    //
    color("salmon") {
        translate([0, 0, chamfer - 0.001]) {
            _tube(height - (chamfer * 2) + (0.001 * 2), od, 0);
        }
    }

    
}

module closed_chamfered_tube(height, od, id, chamfer) {
    chamfered_tube(height, od, id, chamfer);

    chamfered_cylinder((od - id) / 2, od, chamfer);
    
}





chamfered_tube(10, 40, 30, 2);
chamfered_tube(20, 60, 50, 1);


chamfered_cylinder(20, 10, 1);


translate([100, 0, 0]) {
    closed_chamfered_tube(30, 60, 40, 2);
}


translate([200, 0, 0]) {

    inch = 25.4;
    eighth = inch / 8;
    
    diameter_in_inches = 1;
    height_in_inches = 1;
    wall_thickness_in_eights = 1;
    
   
    closed_chamfered_tube(height_in_inches * inch, diameter_in_inches * inch, (diameter_in_inches * inch) - (wall_thickness_in_eights * eighth * 2), 1);
}





