use <lib/chamfered.scad>;
use <lib/cover.scad>;

fine = true;
//fine = false;

$fn = fine ? 200 : 12;

roundcorner = 2;
roundcorner_radius = roundcorner / 2;
roundcorner_diameter = roundcorner;

horn_arc = 45;

horn_diameter = 5;

base_size = 25.4;
plate_thickness = base_size / 8;

height = 6;



tube_od = (base_size * 2) - 1;
tube_id = tube_od - (plate_thickness * 2);

tube_length = base_size * height;

translate([base_size / 2, 0, (height * base_size) /*- 0.0341*/])
    rotate([90, 180, 0])
        cover(1, height, 0.2);
        
translate([0, (tube_od / 2) - (plate_thickness - 0.3), 0]) 
    closed_chamfered_tube(height * base_size, tube_od, tube_id, (plate_thickness / 2) / 2);



