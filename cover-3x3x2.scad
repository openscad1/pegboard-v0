fine = true;
//fine = false;

$fn = fine ? 25 : 12;



base_size = 25.4;
plate_thickness = base_size / 8;


fudge = 0.2;

use <lib/cover.scad>;

width = 3;
height = 3;

color("purple")
translate([-1 * width * (base_size / 2), -1 * height * (base_size / 2),  -1 * plate_thickness * 3 / 2]) {
    for(offset = [0:2]) {
        translate([base_size * offset, 0, 0]) {
            cover(1, height, fudge);
        }
    }
}


color("orange")
translate([0, 0, -1 * ((plate_thickness * 2) + fudge)])
    rotate([0, 180, 180]) 
        translate([-1 * width * (base_size / 2), -1 * height * (base_size / 2), -1 * plate_thickness * 3 / 2]) 
            cover(width, height, fudge);


