// render with fine detail
fine = true;
//fine = false;

// fine detail number of facets
fn_fine = 25;

// low detail number of facets
fn_no_fine = 12;

$fn = fine ? fn_fine : fn_no_fine;



// radius of square edges
roundcorner_radius = 1;
roundcorner = roundcorner_radius * 2;
roundcorner_diameter = roundcorner;



base_size = 25.4 + 0;
plate_thickness = base_size / 8;

use <lib/cover.scad>;
use <lib/tube.scad>;

tube_id = 22.25;
tube_od = tube_id + (plate_thickness * 2);

width = 3; //[0,1,2]

height = 2; //[0,1,2]





//cube([base_size * width, 10, 10]);

rotate([90, 0, 0]) {
    for(pos = [0:2]) {
        rotate([-90, 0, 0]) {
            translate([(base_size * pos), 0, 0]) {
                cover(1,height, 0.2);
            }
        }
        translate([(base_size * pos)+ (base_size / 2), (- tube_od / 2) + 2.4, - (base_size * height)]) {
            hollow_tube(od = tube_od, id = tube_id, h = (base_size * height), r = 1);
            solid_tube(od = tube_od, h = plate_thickness, r = 1);
        }
    }
}



