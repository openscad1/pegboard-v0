// BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

include <ruler-holder.scad>;
width_factor = 1.5;
length_factor = 5;
height_factor = 5;

render_ruler_holder();

// EOF
