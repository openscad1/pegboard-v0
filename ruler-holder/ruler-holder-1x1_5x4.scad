// BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

include <ruler-holder.scad>;


height_factor = 4;
length_factor = 1.5;
width_factor = 1;

render_ruler_holder();

// EOF
