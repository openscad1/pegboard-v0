// BOF
// NOSTL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

tube_id = 28;
tube_od = tube_id + (wall_thickness * 2);

// height = 5;  // 5 for small box cutters
height = 1;  // 1 for test print

spread_factor = 1.35;

//color("red") chamfered_box([base_size, base_size, base_size * 4], align = [-2, 0.75, 0], chamfer = wall_thickness / 4);

module tuber(copies = 1, shift = 0) {
    translate([shift, 0, 0]) {
        color("blue") {
            closed_chamfered_tube(base_size * height, tube_od, tube_id, align = [0, 1, 1], chamfer = wall_thickness / 4, fn = 150);
        }
    }
    if(copies) {
        color("orange") {
            translate([0, -0.5, 0]) {
                rail(copies = copies, height = height, chamfer = wall_thickness / 4, side_wall_gap = default_back_wall_gap);
            }
        }
    }
}


translate([base_size * spread_factor * 0, 0, 0]) tuber(copies = 0, shift =  1.2);
translate([base_size * spread_factor  * 1, 0, 0]) tuber(copies = 4, shift =  0.0);
translate([base_size * spread_factor  * 2, 0, 0]) tuber(copies = 0, shift = -1.2);



//module closed_chamfered_tube(height, od, id, align = [0, 0, 1], chamfer = 0, cutout = false, fn = 0) {

// translate([0, 0, -20]) cube([25.4 * 3, 10, 40]);

// EOF
