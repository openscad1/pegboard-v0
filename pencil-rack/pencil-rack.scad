// NOSTL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

height_factor = 4;
width_factor = 4;

side_allowance = 0.25;

stack_height = 3;

tube_height = base_size * stack_height;
tube_width = base_size;
tube_wall_thickness = wall_thickness;
tube_chamfer = tube_wall_thickness / 4;


$fn = 150;




module rack(w = width_factor, squash_base = false) {

    intersection() {


union() {
        rotate([-30, 0, 0]) {

            for (x = [0 : (w - 1)]) {

                ff = 0.95;
                
                translate([-(base_size * ff * (w - 1))/2, 0, 0]) {
                    translate([x * base_size * ff, 0, 0]) {
                        color("orange") {
                            closed_chamfered_tube(height = stack_height * base_size, od = tube_width, id = tube_width - (tube_wall_thickness * 2), align = [0, 1, 1], chamfer = tube_chamfer);
                        }
                        color("pink") {
                            translate([0, 0, wall_thickness - base_size * 2]){
                                chamfered_cylinder(height = (base_size * 2) - 0.01, od = tube_width, align = [0, 1, 1], chamfer = tube_chamfer);
                            }
                        }

                    }
                }
            }
        }
    }
        translate([0, wall_thickness / 2, (base_size * -0.5)]) {

            color("red") {
                if(squash_base) {
                    chamfered_box([tube_width * width_factor, tube_width * 4, base_size * 8], align = [0, 1, 1]);
                } else {
                    chamfered_box([tube_width * width_factor, tube_width * 4, base_size * 8], align = [0, 1, 0.5]);
                }
            }
        }
    }
}



module render(thirdrow = true) {
    color("blue") {
    rail(height = thirdrow ? height_factor : height_factor - 1, copies = width_factor, chamfer = tube_chamfer, side_wall_gap = side_allowance);
}

    translate([0, 0, base_size * 0.5]) {
        foo = 1.7;
        rack(squash_base = true);
        translate([0, 0, foo * base_size]) rack(w = width_factor - 1);
        if(thirdrow) {
            translate([0, 0, foo * 2 * base_size]) rack();
        }



    }
}

//render();

//EOF
