// NOSTL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

h = 2;
w = 2;
d = 2;

reflection = -1;

color("lightblue") {
    rotate([0, 0, 180]) {
        translate([0, -fudge_factor, -(base_size * h) / 2]) {
            rail(h, copies = w, chamfer = chamfer);
        }
    }
}



for(r = [-1, 1]) {

    color("orange") {
        translate([w * (base_size / 2) * reflection * r, -base_size * d, 0]) {
            hull() {
                chamfered_box(dim = [wall_thickness * w * 2, wall_thickness * 1, base_size * h    ], align=[-1 * reflection* r,1,0], chamfer = chamfer);
                chamfered_box(dim = [wall_thickness * w * 3, wall_thickness * 1, base_size * h / 2], align=[-1 * reflection* r,1,0], chamfer = chamfer);
                chamfered_box(dim = [wall_thickness * w * 1.125, wall_thickness * 2, base_size * h / 2], align=[-1 * reflection* r,1,0], chamfer = chamfer);

            }
        }

    }
    color("grey") {
        translate([w * (base_size / 2) * reflection * r, 0, 0]) {
            hull() {
                chamfered_box(dim = [wall_thickness * 1, base_size * d, base_size * h], align = [-1 * reflection * r, -1, 0], chamfer = chamfer);
                chamfered_box(dim = [wall_thickness * 2, base_size * d, base_size * h/2], align = [-1 * reflection * r, -1, 0], chamfer = chamfer);
            }

        }
    }
}
