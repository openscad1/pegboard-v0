base_size = 25.4;
plate_thickness = base_size / 8;

tube_id = 60;
tube_od = tube_id + (plate_thickness * 2);

use <lib/cup_holder.scad>;
cup_holder(tube_id = tube_id, tube_od = tube_od, magnitude = 3);

