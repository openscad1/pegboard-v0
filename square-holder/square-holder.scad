//BOF
//NOSTL
include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

height_factor = 2;
height = base_size * height_factor;

reach_factor = 3;
reach = base_size * reach_factor;

stop_factor = 1.5;
stop = base_size * stop_factor;

ruler_blade_depth = 1.5;

chamfer = wall_thickness / 4;

color("royalblue") {
    rail(height_factor, chamfer = chamfer);
}

module blade() {
    color("orange") {
        hull() {
            chamfered_box([wall_thickness, wall_thickness, height], align = [0, 1, 1], chamfer = chamfer);
            translate([0, reach - wall_thickness, 0]) {
                chamfered_box([wall_thickness, wall_thickness, height/2], align = [0, 1, 3], chamfer = chamfer);
            }        
        }
    }
}

for(xoff = [-1, 1]) {
    echo(xoff);
    translate([wall_thickness * xoff, 0.01, 0]) {
        blade();
    }
}

color("magenta") {
    translate([0, stop - wall_thickness, height]) {
//        chamfered_box([wall_thickness * 3, reach - stop + wall_thickness, wall_thickness], align = [0, 1, -1], chamfer = chamfer);
    }
}

color("salmon") {
    translate([0, reach - wall_thickness, 0]) {
//        chamfered_box([wall_thickness * 3, wall_thickness, height/2], align = [0, 1, 3], chamfer = chamfer);
    }
}




skid_plate_length = sqrt(pow((height_factor / 2), 2) + pow(reach_factor, 2));

skid_plate_angle = asin((height_factor / 2) / (skid_plate_length - (wall_thickness / base_size)));

echo(skid_plate_length, skid_plate_angle);
clip = ((ruler_blade_depth + 1/8) /cos(skid_plate_angle));

color("red") {
    translate([0, reach, height / 2]) {
        rotate([skid_plate_angle, 0, 0]) {
            chamfered_box([wall_thickness * 3, (skid_plate_length - clip) * base_size - wall_thickness, wall_thickness], align = [0, -1, 1], chamfer = chamfer);
        }
    }
}



// true
// false

slug = false;

if(slug) {
    color("cyan") {
        translate([0, wall_thickness,0]) {
            chamfered_box([wall_thickness, ruler_blade_depth * base_size, 150], align = [0, 1, 0.5], chamfer = chamfer);
        }
    }
}




//EOF
