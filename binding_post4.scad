use <lib/cover.scad>;
use <lib/moscube.scad>;

fine = true; 
//fine = false; 

$fn = fine ? 25 : 12;

base_size = 25.4; 



module post() {
    scale([1,1,1]) {
        minkowski() {
            union() {
                cylinder(d=3, h=14);
                translate([0,0,12]) {
                    cylinder(d=7.75, h =2); 
                }
            }
            sphere(r=1);
        }
    }
}








cover_length = 2;




/*
translate([base_size/4, base_size/2, 15]) color("red") moscube([3,3,3], roundcorner_radius = 1);
translate([0, 0, 10]) rotate([0, 90, 0]) color("cyan") cylinder(d = 3, h = 30);
translate([0, 0, 10]) rotate([90, 0, 180]) color("cyan") cylinder(d = 3, h = 30);
*/


coverx = 2;
covery = 1;

post_per_cover = 2;


rotate([180, 0, 90]) {
    for(y = [1:covery]) {
        translate([(y-1) * base_size, 0, 0]) {
            cover(1, coverx, 0.2);
        }
    }
}


for(xoff = [0 : (coverx - 1)]) {
    for(yoff = [0 : (covery - 1)]) {
        translate([
            ((base_size / post_per_cover) / 2) + (xoff * base_size), 
            ((base_size / post_per_cover) / 2) + (yoff * base_size), 
            0
        ]) {
            for(xp = [0 : (post_per_cover - 1)]) {
                for(yp = [0 : (post_per_cover - 1)]) {
                    translate([xp * (base_size / post_per_cover), yp * (base_size / post_per_cover), 0]) {
                        post();
                    }
                }
            }
        }
    }
}

