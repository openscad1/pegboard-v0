// BOF
// NO STL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

height_factor = 6;
length_factor = 2;
width_factor = 1;


height = base_size * height_factor;

//outside_length = base_size * length_factor;
//inside_length = outside_length - (wall_thickness * 2);
inside_length = 20;
outside_length = inside_length + (wall_thickness * 2);

//outside_width = (base_size * width_factor) - default_back_wall_gap;
//inside_width = outside_width - (wall_thickness * 2);
inside_width = 8;
outside_width = inside_width + (wall_thickness * 2) - default_back_wall_gap;

chamfer = wall_thickness / 4;

inside_radius = wall_thickness * 1;


$fn = 200;


// true
// false

label = false;


module body() {
    difference() {    
        union() {
            translate([0, outside_length / 2 + 0.01, 0.005]) {  // 0.005 ... wth?
                translate([0, 0, height / 2]) {
                    radiused_box(inside_height = height, inside_length = inside_length, inside_width = inside_width, inside_radius = inside_radius, wall_thickness = wall_thickness, color = "pink", chamfer = chamfer);
                }
                translate([0, 0, wall_thickness / 2]) {
                    hull() {
                        radiused_box(inside_height = wall_thickness, inside_length = inside_length, inside_width = inside_width, inside_radius = inside_radius, wall_thickness = wall_thickness, color = "pink", chamfer = chamfer);
                    }
                }
            }
        }    
        if(label) {
            color("cyan") {
                translate([0, outside_length / 2, -wall_thickness / 4]) {
                    rotate([0, 0, 90]) {
                        linear_extrude(wall_thickness /2) {
                            mirror([0, 1, 0]) {
                                fontsize = 5;
                                text(str("L", length_factor, "  H", height_factor, "  W", width_factor), valign = "center", halign = "center", size = fontsize);
                            }
                        }
                    }
                }
            }
        }
    }
}

module mount() {
    color("lightblue") {
        rail(height = height_factor, copies = floor(width_factor), side_wall_gap = default_back_wall_gap, chamfer = chamfer);
    }
}




difference () {
union() {
    body();
    mount();
}


color("red") {
    translate([0, 0, -499.99]) {
        cube([1000, 1000, 1000], center = true);
    }
}
}



// EOF
