// BOF

// NO STL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

h = 4;
height = h * base_size;


tube_id = (base_size * 1.25);
tube_od = tube_id + (wall_thickness * 2);

echo("tube_id:", tube_id);
echo("tube_od:", tube_od);

chamfer = wall_thickness / 4;


difference () {
    translate([0, 0, height])
    rotate([0, 180, 0])
    union() {

        color("cyan") {
            // holder tube
            chamfered_tube(height, tube_od, tube_id, align = [0, 1, 1], chamfer = chamfer, cutout = false, fn = 200);
        }

        color("coral") {
            // mounting rail
            translate([0, -base_size, 0]) {
                // distance the manifold head from the mounting surface
                rail(h, chamfer = chamfer);
            }
        }
        

        difference() {
            // fill the span between the mounting rail and the holder tube
            color("green") {
                translate([0, wall_thickness * 2 + 1, 0]) {
                    chamfered_box(dim = [base_size * 0.75, base_size * 1.25, height - 0.01], align = [0, -1, 1], chamfer = chamfer);
                }
            }
            color("purple") {
                scale([0.95, 0.95, 1]) hull()
                chamfered_tube(height, tube_od, tube_id, align = [0, 1, 1], chamfer = chamfer, cutout = false, fn = 200);
            }
        }
    }


    color("red") {
        // trim the bottom to work around "hovering" above the base plate
        // FIXME
        translate([0, 0, -499.99]) {
            cube([1000, 1000, 1000], center = true);
        }
    }
    
}

// EOF
