//NOSTL

include <defaults.scad>;
use <mostube/mostube.scad>;
use <moscube/moscube.scad>;
use <lib/cover.scad>;

fine = true;
//fine = false;

$fn = fine ? 25 : 12;


height = 4.25;
roundiness = 1;
depth = 2.425;
width = 1;


tube_od = (base_size * 2 - 1);
tube_id = tube_od - (plate_thickness * 2);
tube_length = base_size * height;




// orange for the slug
//
slug_x = 2.05;
slug_y = 0.755;
color("orange") {
    translate([0, (slug_x * -base_size / 2) - 4, base_size * 3]) {
        //cube([slug_y * base_size, slug_x * base_size, base_size * 6], center = true);
    }
}

// cyan for the padded slug
//
color("cyan") {
    translate([0, (slug_x * -base_size / 2) - 4, base_size * 3]) {
        //cube([slug_y * base_size + (plate_thickness) -2, slug_x * base_size + plate_thickness, base_size * 3], center = true);
    }
}

module scaffold() {
    difference() {
        color("purple") union() {
            translate([0, 0, roundiness + (tube_length / 2)]) {
                difference() {
                    cube([(base_size * width) - (roundiness * 2), (base_size * depth) - (roundiness * 2), tube_length - (roundiness * 4)], center = true);
                    moscube([(base_size * width) - (plate_thickness * 2) + (roundiness * 2) + 1, (base_size * depth) - (plate_thickness * 2) + (roundiness * 2), tube_length], roundcorner_radius = roundiness);
                }
            }                   
            translate([0, 0, (plate_thickness / 2) + roundiness]) {
                cube([(base_size * width) - (roundiness * 2), (base_size * depth) - (roundiness * 2), plate_thickness], center = true);
            }
        }
        
        gap_factor = 2/3;
        translate([0, -(depth * base_size) / 2, (base_size * height) / 2]){
            scale([1, 1, height * gap_factor]) {
                rotate([90, 0, 90]) {
                    color("coral") cylinder(d = base_size, h = tube_od + 1, center = true);
                }
            }
        }
    }    
}



intersection() {

    translate([0, 0, 500.04]) {
        cube([1000, 1000, 1000], center = true);
    }

    union() {
        //rc = false;
        rc = true; 

        color("red") translate([0, 0, 0]) {
            translate([0, -(depth * base_size) / 2, 0]) {
                if(rc) {
                    minkowski() {
                        scaffold();
                        sphere(r = roundiness);
                    }
                } else {
                    scaffold();
                }    
            }
        }


        color("plum") 
        translate([-base_size/2, -plate_thickness/2 - 1, base_size * height]) {
            rotate([270, 0, 0]) {
                cover(1, height, 0.2);
            }
        }
    }
}



