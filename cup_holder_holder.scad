hull() {
    cylinder(d = 50, h = 1);
    translate([0, 25, 15]) cube([15, 1, 30], center = true);
}