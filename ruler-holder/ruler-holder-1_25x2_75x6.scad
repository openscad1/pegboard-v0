// BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

include <ruler-holder.scad>;
height_factor = 6;
length_factor = 2.75;
width_factor = 1.25;

render_ruler_holder();

// EOF
