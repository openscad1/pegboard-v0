facets = 25;
$fn = facets;

fudge_factor = 1;
width = 91 + fudge_factor;
depth = 100 + fudge_factor;
height = 10;

edginess = 3;

tray_wall_thickness = 25.4 / 8;

module fillet_edge() {
    color("seagreen") {
        sphere(d = edginess);
    }
}

module chamfer_edge() {
    resize([edginess, edginess, edginess]) {
      rotate([45, 45, 45]) {
          color("teal") {
              cube([edginess, edginess, edginess], center=true);
          }
      }
  }
}


module hard_slug() {
    color("red") {
        translate([0, 0, - height / 2]) {
            cube([depth, width, height], center = true);
        }
    }
}


module soft_slug() {
    color("orange") {
        translate([0, 0, - height / 2]) {
            minkowski() {
                cube([depth - (edginess), width - (edginess), height - (edginess)], center = true);
                chamfer_edge();
            }
        }
    }
}





module shelf() {
    color("violet") {
        translate([0, 0, - (height + tray_wall_thickness) / 2]) {
            minkowski() {
                cube([tray_wall_thickness + depth - (edginess), tray_wall_thickness + width - (edginess), tray_wall_thickness + height - (edginess)], center = true);
                chamfer_edge();
            }
        }
    }
}



//hard_slug();

difference() {
    shelf();
    soft_slug();
}