// BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

include <ruler-holder.scad>;
height_factor = 5;
length_factor = 2.25;
width_factor = 1.5;

render_ruler_holder();

// EOF
