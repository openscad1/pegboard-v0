use <lib/moscube.scad>;


spindle_diameter = 7.25;
spindle_threaded_portion = 40;

octopus_height = 46.6;

tap_diameter = 4.2;

$fn = 25;

difference() {
    scale([1,1,0.75]) import("lib/octopus4.stl");
    color("blue") moscube([spindle_diameter,spindle_diameter,spindle_threaded_portion], roundcorner_radius = 0.5, z_anchor = 1);
    translate([0, 0, octopus_height / 2]) {
        rotate([0,90,90])
        color("red") cylinder(d = tap_diameter, h = 50);
    }
}