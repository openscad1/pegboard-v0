include <pencil-case.scad>;

width_factor = 3;
do_cap = false;
do_base = true;

offset = tube_wall_thickness / 4;


// magnet_pocket = false;


module tri_base() {
    difference() {
        union() {
            for (a = [0, 120, 240]) {
                rotate([0, 0, a]) {
                    translate([((tube_width + (tube_wall_thickness * 2)) / 2) + offset, 0, 0]) {
                        translate([0, (tube_width + (tube_wall_thickness * 2)) / 2, 0]) {
                            render(1);
                        }
                    }
                }
            }
        }
        
        

        if(magnet_pocket) {
            translate([0, 0, (tube_height/ 2) - (inch * 0.5) + 0.01]) {
                magnet_slug();
            }
        }

    }
} 


// debug = true;
//debug2 = true;
debug2 = false;

intersection() {
    tri_base();
    if(debug) {
        cylinder(d = 20, h = 65);
    }
    if(debug2) {
        translate([0, 0, tube_height * 1/2]) {
            cube([200, 200, 20], center = true);
        }
    }

}




// EOF
