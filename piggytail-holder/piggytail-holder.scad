// BOF

// NO STL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

h = 6;
height = h * base_size;

tube_od = (base_size * 2 - 1);
tube_id = tube_od - (wall_thickness * 2);

chamfer = wall_thickness / 4;


difference () {
    union() {
        color("cyan")
        closed_chamfered_tube(height, tube_od, tube_id, align = [0, 1, 1], chamfer = chamfer, cutout = false, fn = 200);

        color("coral")
        rail(h, chamfer = chamfer);
    }


    color("red") {
        translate([0, 0, -499.99]) {
            cube([1000, 1000, 1000], center = true);
        }
    }
    
}

// EOF
