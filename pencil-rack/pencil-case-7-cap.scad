include <pencil-case.scad>;

width_factor = 3;
do_cap = true;
do_base = false;

offset = (tube_wall_thickness / 4) + (tube_width / 2);

tubes = 6;

// default from pencil-case.scad (inside of cap tube): gap = 0.25;
// default from pencil-case.scad (outside of base inner tube): outer_gap = 0;

gap = 0.75;

intersection() {
    union() {
        difference() {
            for (a = [0 : 360 / tubes : 359]) {
                rotate([0, 0, a]) {
                    translate([((tube_width + (tube_wall_thickness * 2)) / 2) + offset, 0, 0]) {
                        translate([0, (tube_width + (tube_wall_thickness * 2)) / 2, 0]) {
                            render(1);
                        }
                    }
                }
            }


        }

        translate([0, (tube_width + (tube_wall_thickness * 2)) / 2, 0]) {
            render(1);
        }
    }

}

// EOF
