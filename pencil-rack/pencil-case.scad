// NOSTL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

width_factor = 1;


tube_wall_thickness = wall_thickness;

interior_height = 6 + (5/16); // enough to hold a retractable sharpie
//interior_height = 1; // test part

tube_height = base_size * (((interior_height * 2) + (tube_wall_thickness / base_size) * 4) / 3);

tube_width = base_size;
tube_chamfer = tube_wall_thickness / 4;
spacing = tube_width + (wall_thickness * 2);

$fn = 150;

/* 
settings "A": initial multicolor PVB case for Henry, tubes too tight - 2022.01

settings "B": large increase in gap (2x) to fit "B" base/cap into "A" - expect final PVB settings to be 1/2 between "A" and "B"


*/

//base_outer_gap = 0; cap_interior_gap = 0.25; // settings: A

base_outer_gap = 0.5; cap_interior_gap = 0.75; // settings: B




ff = 0.95;


    // true
    // false

    magnet_pocket = true;


    // true
    // false
    debug = false;

module base(width_factor) {

    for (x = [0 : (width_factor - 1)]) {

            
        translate([-(spacing * ff * (width_factor - 1))/2, -spacing / 2, 0]) {
            translate([x * spacing * ff, 0, 0]) {
                color("orange") {
                  closed_chamfered_tube(height = tube_height, od = tube_width - base_outer_gap, id = tube_width - (tube_wall_thickness * 2), align = [0, 0, 1], chamfer = tube_chamfer);
                }
 
                    color("salmon") {
                    chamfered_tube(height = tube_height/ 2, od = tube_width + (tube_wall_thickness * 2), id = tube_width - (tube_wall_thickness * 2) + 0.02, align = [0, 0, 1], chamfer = tube_chamfer * 2);
            }

            }
        }
    }
}


module cap(width_factor = width_factor) {
    
    for (x = [0 : (width_factor - 1)]) {

            
        translate([-(spacing * ff * (width_factor - 1))/2, -spacing / 2, 0]) {
            translate([x * spacing * ff, 0, 0]) {
                    color("tan") {
                        chamfered_tube(height = tube_wall_thickness * 1.5, od = tube_width + (tube_wall_thickness * 2), id = tube_width - (tube_wall_thickness * 1) + 0.25, align = [0, 0, 1], chamfer = tube_chamfer * 2);
                        }

                    color("grey") {
                        translate([0, 0, tube_wall_thickness/2]) {
                            chamfered_tube(height = tube_height - tube_wall_thickness/2, od = tube_width + (tube_wall_thickness * 2), id = tube_width + cap_interior_gap, align = [0, 0, 1], chamfer = tube_chamfer);
                        }
                    }
                    color("purple") {
                        chamfered_cylinder(height = tube_wall_thickness, od = tube_width+ (tube_wall_thickness), align = [0, 0, 1], chamfer = tube_chamfer);
                    }

                }

        }
    }

}


// true
// false
do_clip = false;
do_cap = true;
do_base = true;


module render(width_factor = width_factor) {
    
    if (do_base) {
        difference() {
            base(width_factor);
            if (do_clip) {
                translate([100, 0, 100]) {
                    color("red") cube([200, 200, 201], center = true);
                }
            }
        }
    }
    
    if (do_cap) {
        difference() {
            translate([0, base_size * 0,  tube_height * 1.5]) {
                rotate([0, 180, 0]) {
                    cap(width_factor);
                }
            }
            if (do_clip) {
                translate([100, 0, 100]) {
                    color("cyan") cube([200, 200, 201], center = true);
                }
            }
        }
    }

}




module magnet_slug() {

    inch = 25.4;
    
    color("red") {
        cylinder(d = inch * 0.25, h = inch * 1, center = true);
    }
}

//magnet_slug();


//render();

// EOF
