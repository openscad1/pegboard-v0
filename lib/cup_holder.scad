

roundcorner = 2;
roundcorner_radius = roundcorner / 2;
roundcorner_diameter = roundcorner;







use <cover.scad>;
use <tube.scad>;


module cup_holder(tube_id = 60, tube_od = 60 + (25.4 * 2)/8, magnitude = 3, plate_height = 0) {

    base_size = 25.4;
    plate_thickness = base_size / 8;

    tube_length = (base_size*magnitude);

    translate([-base_size / 2, tube_length-(base_size*magnitude), 0]) {
        if(0 == plate_height) {
            cover(1, magnitude, 0.2);
            translate([base_size, 0, 0]) 
                cover(1, magnitude, 0.2);            
        } else {
            cover(1, plate_height, 0.2);
            translate([base_size, 0, 0]) 
                cover(1, plate_height, 0.2);

        }
    }
    rotate([90, 0, 0]) {
        translate([base_size / 2, (- tube_od / 2) + 2.4, - (tube_length)]) {
            hollow_tube(od = tube_od, id = tube_id, h = (tube_length), r = 1);
            solid_tube(od = tube_od, h = plate_thickness, r = 1);
        }
    }
}





module ch2(tube_id = 60, tube_od = 60 + ((25.4 / 8) * 2), magnitude = 3, plate_height = 0, slope = 1.0) {
    base_size = 25.4;
    plate_thickness = base_size / 8;

    tube_length = (base_size*magnitude);

    translate([-base_size / 2, tube_length-(base_size*magnitude), 0]) {
        if(0 == plate_height) {
            cover(1, magnitude, 0.2);
            translate([base_size, 0, 0]) 
                cover(1, magnitude, 0.2);            
        } else {
            cover(1, plate_height, 0.2);
            translate([base_size, 0, 0]) 
                cover(1, plate_height, 0.2);

        }
    }
    rotate([90, 0, 0]) {
        translate([base_size / 2, (- tube_od / 2) + 3, - (tube_length)]) {

            
            rcr = 1;
            rcd = 2;

            translate([0, 0, rcr]) 
            minkowski()  {
                difference() {
                    difference() {
                        cylinder(d = tube_od - rcd, h = tube_length - rcd);
                        cylinder(d = tube_id + rcd, h = tube_length);
                    }
                    slab_thickness = 50;
                    rotate([22.5, 0, 0]) {
                        translate([0, 0, (slab_thickness / 2) + (base_size * magnitude) - 15]) {
                            cube([100, 100, slab_thickness], center = true);
                        }
                    }
                }
                sphere(r = rcr);
            }
            
            color("orchid")
                solid_tube(od = tube_od, h = plate_thickness, r = 1);
        }
    } 
}

module ch3(tube_id = 60, tube_od = 60 + ((25.4 / 8) * 2), magnitude = 3, plate_height = 0, slope = 1.0) {
    base_size = 25.4;
    plate_thickness = base_size / 8;

    tube_length = (base_size*magnitude);

    translate([-base_size / 2, tube_length-(base_size*magnitude), 0]) {
        if(0 == plate_height) {
            cover(1, magnitude, 0.2);
            translate([base_size, 0, 0]) 
                cover(1, magnitude, 0.2);            
        } else {
            cover(1, plate_height, 0.2);
            translate([base_size, 0, 0]) 
                cover(1, plate_height, 0.2);

        }
    }
    rotate([90, 0, 0]) {
        translate([base_size / 2, (- tube_od / 2) + 3, - (tube_length)]) {

            
            rcr = 1;
            rcd = 2;

            translate([0, 0, rcr]) 
            minkowski()  {
//            union() {
                difference() {
                    difference() {
                        cylinder(d = tube_od - rcd, h = tube_length - rcd);
                        cylinder(d = tube_id + rcd, h = tube_length);
                    }
                    slab_thickness = 50;///*50*/ 1;
                    rotate([slope, 0, 0]) {
                        translate([0, 0, (slab_thickness / 2) + (base_size * magnitude) - 30 + 10]) {
                            cube([200, 200, slab_thickness * magnitude / 2] , center = true);
                        }
                    }
                }
                sphere(r = rcr);
            }
            
            color("orchid")
                solid_tube(od = tube_od, h = plate_thickness, r = 1);
        }
    } 
}


/*
for(m = [1:4]) {
    translate([100 * (m-1), 0, 0]) {
        echo(m);
        cup_holder(tube_id = 60, tube_od = 60 + (25.4 * 2)/8, magnitude = m);
    }
}

translate([-100, 0, 0]) cup_holder(magnitude = 1, plate_height = 2);
*/

//ch2(magnitude=2, slope = 0.5);

/* ch3
base_size = 25.4;
plate_thickness = base_size / 8;

tube_id = 110;
tube_od = tube_id + (plate_thickness * 2);

rotate([-90, 0, 0])
        ch3(tube_id = tube_id, tube_od = tube_od, magnitude = 3, slope = 33);
*/