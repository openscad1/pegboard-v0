include <defaults.scad>;
use <mostube/mostube.scad>;
use <lib/cover.scad>;


tube_od = (base_size * 2 - 1);
tube_id = tube_od - (plate_thickness * 2);
tube_length = base_size * height;




module scaffold() {
    difference() {
        union() {
            color("purple") mostube(outer_diameter = tube_od, inner_diameter = tube_id, height = tube_length, gap = roundiness, z_anchor = 1);
            mostube(outer_diameter = tube_od, height = plate_thickness, gap = roundiness, z_anchor = 1);
        }
        
        gap_factor = 2/3;
        translate([0, -tube_od / 2, (base_size * height) / 2]){
            scale([1, 1, height * gap_factor]) {
                rotate([90, 0, 90]) {
                    color("coral") cylinder(d = base_size, h = tube_od + 1, center = true);
                }
            }
        }
    }    
}



//rc = false;
rc = true; 

translate([0, 3, 0]) {
    translate([0, -tube_od / 2, 0]) {
        if(rc) {
            minkowski() {
                scaffold();
                sphere(r = roundiness);
            }
        } else {
            scaffold();
        }    
    }
}

translate([-base_size/2, 0, base_size * height]) {
    rotate([270, 0, 0]) {
        cover(1, height, 0.2);
    }
}
