// BOF
// NOSTL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

chamfer = wall_thickness / 4;



module 30_degree_bracket(total_height = 3, copies = 1) {
    
    
    for(i = [1 : copies]) {
        
        translate([0, (base_size - 1) * i, 0]) {


            translate([-wall_thickness - 0.01, 0, base_size]) {
                rotate([0, 0, -90]) {
                    rail(total_height - 1, chamfer = chamfer);
                }
            }



            // fin
            //
            color("orange") {
                hull() {
                    rotate([0, 30, 0]) {
                        chamfered_box(dim = [wall_thickness, wall_thickness, base_size * total_height], align = [-1, 0, 1], chamfer = chamfer);
                    }
                    chamfered_box(dim = [wall_thickness, wall_thickness, base_size * total_height], align = [-1, 0, 1], chamfer = chamfer);
                }
            }


            // rail head
            //
            rotate([0, 30, 0]) {
                color("salmon") {
                    chamfered_box(dim = [wall_thickness, base_size - (wall_thickness * 2) - (default_back_wall_gap * 2), base_size * total_height], 
                        align = [-1, 0, 1], 
                        chamfer = chamfer);
                }
            }
        }
    }
}


//30_degree_bracket(copies = 3);


// EOF
