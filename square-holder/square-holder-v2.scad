// BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;
include <mcube.scad>;

include <square-holder-v2-lib.scad>;
height_factor = 3;
length_factor = 1.5;
width_factor = 1;

width_scale = 0.5;
width_scalex = 1.3;
label = false;


debug = true; 

//debug_the_plug = true;


if(!is_undef(debug) && debug) {
    $fn = 10;
}

extra_width = 0;

width = [17, 19, 20];

module bracket(width = base_size / 2) {

    echo(width);

    foo = 2;
    bar = width + chamfer * 2;
    baz = 8;  // width of interior slot for ruler of square

    base_offset = 4;

    
    difference() {
        
        
        union() {

            if(is_undef(debug_the_plug) || !debug_the_plug) {
                
                color("orange") {

                    hull() {
                        
                        setback = 50;
                        
                        // top
                        translate([0, (base_size * 2.25 * .875 ) - setback/2, base_size * 1.5 - wall_thickness * (1.5 - ((4 - foo) / 2))]) {
                            radiused_box(inside_height = wall_thickness * foo, inside_length = base_size * 4.5 * .75 + setback + (base_offset * 2), inside_width = bar, inside_radius = wall_thickness, wall_thickness = wall_thickness, color = "orange", chamfer = wall_thickness / 4);
                        }
                        
                        // bottom
                        translate([0, (base_size * 0.875 ) - setback/2, -base_size * 1.5 + wall_thickness * (1.5 - ((4 - foo) / 2))])  {
                            radiused_box(inside_height = wall_thickness * foo, inside_length = base_size * 1.5 + setback, inside_width = bar, inside_radius = wall_thickness, wall_thickness = wall_thickness, color = "orange", chamfer = wall_thickness / 4);
                        }

                    }
                }
            }

        }
        
        union() {

            guardrail = true;

            guard_rail_height = 3;
            
            if(!is_undef(guardrail) && guardrail) {
                if(is_undef(debug_the_plug) || !debug_the_plug) {

                    color("red") {
                        translate([0, wall_thickness, base_size * 1.5 + wall_thickness /2]) {
                            mcube([bar, 100, guard_rail_height * 2], chamfer = chamfer, align = [0, 1, 0]);
                        }
                        
                        translate([0, wall_thickness, base_size * 1.5 + wall_thickness - chamfer]) {
                            mcube([bar + wall_thickness / 2, 100, wall_thickness], chamfer = chamfer, align = [0, 1, 0]);
                        }


                    }
                }
            }
            
            // bring cutout all the way to the lip
            color("fuchsia")
            translate([0, 17.1, 0]) {
                h = base_size * 3 + wall_thickness + 1;
                hull() {
                    hull() {
                        translate([0, 0, -h/2]) {
                            radiused_box(inside_height = 0.01, inside_length = base_size * 1.5 - 6 +base_offset*2, inside_width = baz, inside_radius = wall_thickness, wall_thickness = 0.01, chamfer = 0);
                        }
                        translate([0, 0, h/2]) {
                            radiused_box(inside_height = 0.01, inside_length = base_size * 1.5 - 6+base_offset*2, inside_width = baz, inside_radius = wall_thickness, wall_thickness = 0.01, chamfer = 0);
                        }
                    }
                    o = 58;
                    translate([0, o/2, h/2]) {
                        radiused_box(inside_height = 0.01, inside_length = base_size * 1.5 - 6 + o+base_offset*2, inside_width = baz, inside_radius = wall_thickness, wall_thickness = 0.01, chamfer = 0);
                    }
                }
            }
        }
    }
    
}
    




intersection() {
    
    union() {
        color("lightblue") {
            translate([0, 1, -(base_size * 1.5 + wall_thickness /2)]) {
                rail(height = 4, copies = 4, side_wall_gap = default_back_wall_gap, chamfer = chamfer);
            }
        }


        intersection() {
            union() {
                for(i = [-1, 0, 1]) {
                    translate([i * width_scalex * base_size, 0, 0]) {
                        bracket(width[i+1]);
                    }
                }
            }
            color("red") translate([0, 2.5, 0]) mcube([500, 500, 500], align = [0, 1, 0]);
        }
        
        
        
//        slug = true;

        if(!is_undef(slug)) {
            translate([0, 20, 76]) {
                for(i = [-1, 0, 1])
                    translate([i * width_scalex * base_size, 0]) {
                    slug_x = 2;
                    slug_y = 30.65;
                    slug_z = 25.4 * 16;
                    color("red") cube([slug_x, slug_y, slug_z], center = true);
                    translate([0, 25.4 + slug_y, 0]) color("cyan") cube([width[i+1], 25.4 *4, 25.4 * 3], center = true);
                }
            }
        }
        
    }
        


 //   slice = true;
    if(!is_undef(slice)) {
        translate([0, 0, 38])
        cube([200, 400, 5], center = true);
    }

}



// EOF
