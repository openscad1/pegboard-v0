include <pencil-case.scad>;


// default from pencil-case.scad (inside of cap tube): gap = 0.25;
// default from pencil-case.scad (outside of base inner tube): outer_gap = 0;

gap = 0.5;
outer_gap = 0.25;

width_factor = 2;
do_cap = true;
do_base = false;

intersection() {
    
    render();

    // slice center for debugging
    //translate([0, 0, 55]) cube([100, 100, 20], center = true);
}
