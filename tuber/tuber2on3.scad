// BOF
// NOSTL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

tube_id = 33;
tube_od = tube_id + (wall_thickness * 2);

// height = 5;  // 5 for small box cutters
height = 1;  // 1 for test print

spread = 0.95;

//color("red") chamfered_box([base_size, base_size, base_size * 4], align = [-2, 0.75, 0], chamfer = wall_thickness / 4);

module tuber() {
        color("blue") {
            closed_chamfered_tube(base_size * height, tube_od, tube_id, align = [0, 1, 1], chamfer = wall_thickness / 4, fn = 150);
    }
}


translate([base_size * -0.75 * spread, 0, 0]) tuber();
translate([base_size *  0.75 * spread, 0, 0]) tuber();

rail(height = height, copies = 3, chamfer = wall_thickness / 4);



// EOF
