// BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

include <ruler-holder.scad>;
height_factor = 3;
length_factor = 2;
width_factor = 1;

render_ruler_holder();

// EOF
