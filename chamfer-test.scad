

module mycube(xyz = [10, 10, 10], x_center = 0, y_center = 0, z_center = 0, chamfer = 0) {
    translate([x_center * (xyz.x / 2), y_center * (xyz.y / 2), z_center * (xyz.z / 2)]) {
        
        difference() {
            color("orange") {
                cube([xyz.x, xyz.y, xyz.z], center = true);
            }
        
            
            // x-axis chamfer
            //
            for(yness = [-1, 1]) {
                for(zness = [-1, 1]) {
                    translate([0, yness * xyz.y / 2, zness * xyz.z / 2]) {
                        rotate([45, 0, 0]) {
                            cube([xyz.x, chamfer + 0.01, chamfer + 0.01], center = true);
                        }
                    }
                }
            }
            
            

            // y-axis chamfer
            //
            for(xness = [-1, 1]) {
                for(zness = [-1, 1]) {
                    translate([xness * xyz.x / 2, 0, zness * xyz.z / 2]) {
                        rotate([0, 45, 0]) {
                            cube([chamfer + 0.01, xyz.y, chamfer + 0.01], center = true);
                        }
                    }
                }
            }




            // z-axis chamfer
            //
            for(xness = [-1, 1]) {
                for(yness = [-1, 1]) {
                    translate([xness * xyz.x / 2, yness * xyz.y / 2, 0]) {
                        rotate([0, 0, 45]) {
                            cube([chamfer + 0.01, chamfer + 0.01, xyz.z], center = true);
                        }
                    }
                }
            }





      }
        
    }
}

mycube([10, 30, 20], chamfer = 4);
