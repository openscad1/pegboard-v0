fine = true;
//fine = false;

$fn = fine ? 25 : 12;

roundcorner = 2;
roundcorner_radius = roundcorner / 2;
roundcorner_diameter = roundcorner;

horn_arc = 45;

horn_diameter = 5;

base_size = 25.4;
plate_thickness = base_size / 8;



use <lib/cover.scad>;
use <lib/tube.scad>;

tube_id = 17;
tube_od = tube_id + (plate_thickness * 2);



plate_factor = 2;
cover(1,plate_factor, 0.2);
rotate([90, 0, 0]) {
    translate([base_size / 2, (- tube_od / 2) + 2.4, - (base_size * plate_factor)]) {
        hollow_tube(od = tube_od, id = tube_id, h = (base_size * (plate_factor - 1)), r = 1);
        solid_tube(od = tube_od, h = plate_thickness, r = 1);
    }
}



