//BOF

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

chamfer = wall_thickness / 4;

width = 4;

translate([-wall_thickness - 0.01, 0, base_size]) {
    rotate([0, 0, -90]) {
        rail(2, copies = width, chamfer = chamfer);
    }
}



for(i = [0:(width -1)]) {
    offset = ((width - 1) / 2) - i;
    echo (i, offset);
    
    translate([0, base_size * offset, 0]) {

        // fin
        //
        color("orange") {
            hull() {
                rotate([0, 30, 0]) {
                    chamfered_box(dim = [wall_thickness, wall_thickness, base_size * 3], align = [-1, 0, 1], chamfer = chamfer);
                }
                chamfered_box(dim = [wall_thickness, wall_thickness, base_size * 3], align = [-1, 0, 1], chamfer = chamfer);
            }
        }


        // rail head
        //
        rotate([0, 30, 0]) {
            color("salmon") {
                chamfered_box(dim = [wall_thickness, base_size - (wall_thickness * 2) - (default_back_wall_gap * 2), base_size * 3], align = [-1, 0, 1], chamfer = chamfer);
            }
        }

    }
}
//EOF
