module moscube(vec = [10, 10, 10], roundcorner_radius = 0, x_anchor = 0, y_anchor = 0, z_anchor = 0) {
    translate([x_anchor * (vec.x/2), y_anchor * (vec.y/2), z_anchor * (vec.z/2)]) {
        minkowski() {
            cube([vec.x - (roundcorner_radius * 2), vec.y - (roundcorner_radius * 2), vec.z - (roundcorner_radius * 2)], center = true);
            sphere(r = roundcorner_radius);
        }
    }
}

$fn = 25;
cube([10, 10, 10], center = true);
translate([ 0, 0, 0]) color(   "red")  moscube(roundcorner_radius = 1);
translate([20, 0, 0]) color( "green")  moscube(roundcorner_radius = 2);
translate([40, 0, 0]) color(  "blue")  moscube(roundcorner_radius = 0.5, x_anchor = -1);
translate([60, 0, 0]) color("orange")  moscube(roundcorner_radius = 0, x_anchor = -1, y_anchor = 1, z_anchor = 1);

