// NO STL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

width_factor = 1;


tube_wall_thickness = wall_thickness;

interior_height = 6 + (5/16); // enough to hold a retractable sharpie
//interior_height = 1; // test part

tube_height = base_size * (((interior_height * 2) + (tube_wall_thickness / base_size) * 4) / 3);

tube_width = base_size;
tube_chamfer = tube_wall_thickness / 4;
spacing = tube_width + (wall_thickness * 2);

$fn = 150;

gap = 0.25;
        ff = 0.95;

module base(width_factor) {

    for (x = [0 : (width_factor - 1)]) {

            
        translate([-(spacing * ff * (width_factor - 1))/2, -spacing / 2, 0]) {
            translate([x * spacing * ff, 0, 0]) {
                color("orange") {
                  closed_chamfered_tube(height = tube_height, od = tube_width, id = tube_width - (tube_wall_thickness * 2), align = [0, 0, 1], chamfer = tube_chamfer);
                }
 
                    color("salmon") {
                    chamfered_tube(height = tube_height/ 2, od = tube_width + (tube_wall_thickness * 2), id = tube_width - (tube_wall_thickness * 2) + gap, align = [0, 0, 1], chamfer = tube_chamfer * 2);
            }

            }
        }
    }
}


module cap(width_factor = width_factor) {
    
    for (x = [0 : (width_factor - 1)]) {

            
        translate([-(spacing * ff * (width_factor - 1))/2, -spacing / 2, 0]) {
            translate([x * spacing * ff, 0, 0]) {
                    color("tan") {
                        chamfered_tube(height = tube_height/2, od = tube_width + (tube_wall_thickness * 2), id = tube_width - (tube_wall_thickness * 2), align = [0, 0, 1], chamfer = tube_chamfer * 2);
                        }

                    color("grey") {
                        translate([0, 0, tube_wall_thickness]) {
                            chamfered_tube(height = tube_height - tube_wall_thickness, od = tube_width + (tube_wall_thickness * 2), id = tube_width + gap, align = [0, 0, 1], chamfer = tube_chamfer);
                        }
                    }
                    color("purple") {
                        chamfered_cylinder(height = tube_wall_thickness, od = tube_width, align = [0, 0, 1], chamfer = tube_chamfer);
                    }

                }

        }
    }

}


// true
// false
do_clip = false;
do_cap = false;
do_base = false;


module render(width_factor = width_factor) {
    difference() {
        union() {
            if (do_base) {
                base(width_factor);
            }
            if (do_cap) {
//                translate([0, base_size * 2,  0]) {
                    cap(width_factor);
//                }
            }
                    translate([-(spacing * ff * (width_factor - 1))/2, -spacing / 2, 0]) {

            //ramrod
            chamfered_cylinder(height = tube_height + base_size*2, od = tube_width - tube_wall_thickness*2, align = [0, 0, 1], chamfer = tube_chamfer);
                        
                        
            }
        }

        if (do_clip) {
            translate([100, 0, 100]) {
                cube([200, 200, 201], center = true);
            }
        }
    }

}



 render();

// EOF
