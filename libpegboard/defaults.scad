// NOSTL
inch = 25.4;
base_size = inch;
wall_thickness = inch / 8;
chamfer = wall_thickness / 4;
default_back_wall_gap = 0.2;
fudge_factor = 0.02;

