base_size = 25.4;
plate_thickness = base_size / 8;

tube_id = 110;
tube_od = tube_id + (plate_thickness * 2);

use <lib/cup_holder.scad>;
use <lib/moscube.scad>;


cable_ports = false;


$fn = 90; 
// $fn = 12; 


rotate([-90, 0, 0])

difference() {

    notch = 40;
    difference() {

        //cup_holder(tube_id = tube_id, tube_od = tube_od, magnitude = 2);
        ch3(tube_id = tube_id, tube_od = tube_od, magnitude = 3, slope = 25);

    }


    if (cable_ports) {
        post_width = 16;

        translate([base_size / 2, base_size * 1.5, -tube_od / 2]) {
            rotate([0, 45, 0]) {
                cylinder(d=post_width, h=100); 
            }
            rotate([0, -45, 0]) {
                cylinder(d=post_width, h=100); 
            }
        }
    }
    
    
    
}

// reference block for cup handle
//
// color("orange") translate([0, -100, base_size * -3]) cube([50, 50, 44], center = true);


/*
if (cable_ports) {
    difference() {
        translate([0, -58, 0]) {
            post_width = 8;

            translate([base_size / 2, base_size * 1.5, -tube_od / 2]) {
                rotate([-45, 45, 0]) {
                    cylinder(d=post_width, h=120); 
                }
                rotate([-45, -45, 0]) {
                    cylinder(d=post_width, h=120); 
                }
            }
        }


        translate([base_size / 2, 75, -((tube_id) / 2)]) {
            rotate([90, 0, 0]) {
                cylinder(d = tube_id - 5, h = 100);
            }
        }
        translate([0, 99.9 + base_size * 2, -50]) {
            rotate([90, 0, 0]) {
                cylinder(d = 200, h = 100);
            }
        }
    }
}
*/
