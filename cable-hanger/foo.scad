// NOSTL

include <../libmos/libchamfer.scad>;
include <../libpegboard/defaults.scad>;
include <../libpegboard/rail.scad>;

h = 2;
w = 2;
d = 2;

reflection = -1;

color("lightblue") {
    rotate([0, 0, 180]) {
        translate([0, -fudge_factor, -(base_size * h) / 2]) {
            rail(h, copies = w, chamfer = chamfer);
        }
    }
}



color("pink") {
    hull(){
        translate([(w * ((base_size / 2) + wall_thickness)) / 2 * reflection, 0, 0]) {
            chamfered_box(dim = [wall_thickness * 2, base_size * d, base_size * h / 4], align = [ 1 * reflection, -1, -2], chamfer = chamfer);
        }
        translate([w * (base_size / 2) * reflection, 0, 0]) {
            chamfered_box(dim = [wall_thickness * 2, base_size * d, base_size * h / 2], align = [-1 * reflection, -1, -1], chamfer = chamfer);
        }
    }
}


color("grey") {
    translate([-w * (base_size / 2) * reflection, 0, 0]) {
        hull() {
            chamfered_box(dim = [wall_thickness * 1, base_size * d, base_size * h], align = [1 * reflection, -1, 0], chamfer = chamfer);
            chamfered_box(dim = [wall_thickness * 2, base_size * d, base_size * h/2], align = [1 * reflection, -1, 0], chamfer = chamfer);
        }

    }
}



color("orange") {
    translate([w * (base_size / 2) * reflection, -base_size * d, 0]) {
        hull() {
            chamfered_box(dim = [wall_thickness * w * 2, wall_thickness * 1, base_size * h    ], align=[-1 * reflection,1,0], chamfer = chamfer);
            chamfered_box(dim = [wall_thickness * w * 4, wall_thickness * 1, base_size * h / 2], align=[-1 * reflection,1,0], chamfer = chamfer);
            chamfered_box(dim = [wall_thickness * w * 1.625, wall_thickness * 2, base_size * h / 4], align=[-1 * reflection,1,-2], chamfer = chamfer);

        }
    }

}


/*
    x = 10;
    y = 2;
    z = 30;
    wall = 2;

    translate([(w * (base_size / 2) - (5 + (x/2)))  * reflection, -base_size * d + -(y + 0.01), 0]) {
        difference() {
            union() {
                color("purple") {
                    chamfered_box(dim = [x + wall * 2,y + wall * 2,z], align = [0, 0, 0], chamfer = 0.5);
                }
                color("green") {
                    hull() {
                        translate([0, 0, -z/2  + wall/2 - 0.01]) {
//                            cube([x + wall * 2 - 1,y + wall * 2 - 1, 0.01], center = true);
             chamfered_box(dim = [x + wall * 2 ,y + wall * 2 , wall + 0.01], align = [0, 0, 0], chamfer = 0.5);

                        }
                        translate([0, y + wall  + .02, -wall*2 -(z/2 + y) + -1]) {
                            cube([x - wall * 4,0.01 ,0.01], center = true);
                        }
                    }
                }
            }


            
            color("red") {
                translate([0, 0.5, 0])
                cube([x, y*2, z + 0.02], center = true);
                translate([0, -1.5, wall *1.5])
                cube([x - 2, y*2, z + 0.02], center = true);

            }
        }
    }


*/