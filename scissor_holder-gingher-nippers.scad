use <lib/loop.scad>;

fine = true;
//fine = false;

$fn = fine ? 50 : 12;


intersection() {
    loop(size = 0.6, height = 3, fn = $fn);
    //cube([100, 1, 100], center = true);
}
